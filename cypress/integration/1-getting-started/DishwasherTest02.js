describe('DishwasherTest02', function() {

    it('Verify all 19 products gets display on Dishwasher Home Page', function()
    {
      //Dishwasher Application Login
      cy.visit('http://localhost:3000/')
      //Title Validation
      cy.title().should('eq','JL & Partners | Home')
      
      //Validating all 19 products getting displayed correctly on Dishwasher Home Page
      cy.contains('Bosch Serie 2 SMV40C30GB Fully Integrated Dishwasher')
      cy.contains('Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher')
      cy.contains('Bosch Serie 2 SMS24AW01G Freestanding Dishwasher, White')
      cy.contains('Bosch Serie 2 SPV2HKX39G Fully Integrated Slimline Dishwasher')
      cy.contains('Beko DFS05020W Freestanding Slimline Dishwasher, White')
      cy.contains('Bosch Serie 6 SMV6ZCX01G Fully Integrated Dishwasher')
      cy.contains('Bosch Serie 6 SMD6ZCX60G Fully Integrated Dishwasher')
      cy.contains('Bosch Serie 2 SMS2ITW08G Freestanding Dishwasher, White')
      cy.contains('Neff N50 S513G60X0G Fully Integrated Dishwasher')
      cy.contains('Beko DFN05320W Freestanding Dishwasher, White')
      cy.contains('Hoover HDI1LO38BS-80 Fully Integrated Dishwasher')
      cy.contains('Miele G5223SC Freestanding Dishwasher, White')
      cy.contains('Bosch Serie 2 SMS2ITI41G Freestanding Dishwasher, Silver')
      cy.contains('Neff N30 S511A50X1G Fully Integrated Dishwasher')
      cy.contains('Neff N30 S153ITX02G Fully Integrated Dishwasher')
      cy.contains('Bosch Serie 4 SMV4HAX40G Fully Integrated Dishwasher')
      cy.contains('Neff N50 S155HCX27G Fully Integrated Dishwasher')
      cy.contains('Bosch Serie 2 SPS2IKW04G Freestanding Slimline Dishwasher, White')
      cy.contains('Bosch Serie 6 SMD6EDX57G Fully Integrated Dishwasher')
   
    })

  })