describe('DishwasherTest01', function() {

    it('Verify Title of the Dishwasher Home Page', function()
    {
      //Dishwasher Application Login
      cy.visit('http://localhost:3000/')
      //Title Validation
      cy.title().should('eq','JL & Partners | Home')
      
      //Validating Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher is getting displayed  on Dishwasher Home Page and priced 599.99
      cy.contains('Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher')
      cy.contains('599.99')

      //Navigating to new screen with dishwasher detail when product is clicked
      cy.contains('Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher').click()
      cy.contains('599.99')
    })

  })