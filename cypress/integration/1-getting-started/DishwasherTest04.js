describe('DishwasherTest04', function() {

    it('Verify product specification details on the new screen ', function()
    {
      //Dishwasher Application Login  
      cy.visit('http://localhost:3000/')
      cy.title().should('eq','JL & Partners | Home')

    //Validating Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher is getting displayed and navigation to details page is successful
      cy.contains('Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher').click()
      
     //Validating all product specification details are listed in detailed  page for Bosch Serie 4 SMV46NX00G Fully Integrated Dishwasher
      cy.contains('Type')
      cy.contains('Noise Level')
      cy.contains('Rinse Aid Indicator')
      cy.contains('Time remaining indicator')
      cy.contains('Dimensions')
      cy.contains('Adjustable racking')
      cy.contains('Digital display')
      cy.contains('Installation Required')
      cy.contains('Delicate Wash')
      cy.contains('Model name / number')
      cy.contains('Program Sequence Indicator')
      cy.contains('Child lock')
      cy.contains('Cable length (m)')
      cy.contains('Cycle duration at rated capacity for the eco cycle')
      cy.contains('Cutlery Holder Type')
      cy.contains('Brand')
      cy.contains('Overflow protection')
      cy.contains('Weight')
      cy.contains('Drying performance')
      cy.contains('Automatic Load Adjustment')
      cy.contains('Drying System')
      cy.contains('Smart Technology')
      cy.contains('Quiet Mark')
      cy.contains('Salt Level Indicator')
      cy.contains('Amperage')
      cy.contains('Energy Rating - Overall')
      cy.contains('Weighted Water Consumption for the Eco cycle')
      cy.contains('Slim depth')
      cy.contains('Place Settings')
      cy.contains('Dishwasher Size')
      cy.contains('Quick wash')
      cy.contains('Number of Programs/Settings')
      cy.contains('Home Appliance Features')
      cy.contains('Weighted Energy Consumption per 100 cycles for Eco cycle')
   
    })

  })