describe('DishwasherTest03', function() {

    it('Verify Hyperlink validation on the new screen', function()
    {
      //Dishwasher Application Login  
      cy.visit('http://localhost:3000/')
      cy.title().should('eq','JL & Partners | Home')

      //Validating Beko DFS05020W Freestanding Slimline Dishwasher is getting displayed and navigation to details page is successful
      cy.contains('Beko DFS05020W Freestanding Slimline Dishwasher, White').click()
     
      //No records found on the detail section, Validating user should be able to move back to Home Page when clicked on Hyperlink
      cy.contains('Homepage').click()
      cy.title().should('eq','JL & Partners | Home')
    })

  })